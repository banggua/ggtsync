package plugin

import (
	"errors"
	"fmt"

	"gitlab.com/banggua/ggtsync/config"
	"gitlab.com/banggua/ggtsync/logger/zap"
	gitlabpush "gitlab.com/banggua/ggtsync/plugin/gitlab-push"
)

// Plugin -
// TODO:
type Plugin interface {
	Init(args []string) error
	Handle(project *config.Project, DstSrcMap map[string]string) error
}

type plugin struct {
	Init   func(args []string) error
	Handle func(project *config.Project, DstSrcMap map[string]string) error
}

type pluginManager struct {
	plugins map[string]*plugin
}

var (
	defaultPluginManager = pluginManager{plugins: make(map[string]*plugin)}
	// ErrorPluginNotSupported -
	// TODO:
	ErrorPluginNotSupported = errors.New("plugin not supported")
)

func init() {
	defaultPluginManager.plugins[gitlabpush.PluginName] = &plugin{Init: gitlabpush.Init, Handle: gitlabpush.Handle}
}

func (pm *pluginManager) Init(plugins []config.Plugin) error {
	var err error
	for _, plugin := range plugins {
		if pmPlugin, exist := pm.plugins[plugin.Name]; exist {
			zap.Infof("%s", plugin.Name)
			err = pmPlugin.Init(plugin.Args)
			if err != nil {
				zap.Errorf("%s %v", plugin.Name, err)
				return fmt.Errorf("%s %w", plugin.Name, err)
			}
			zap.Infof("Done %s", plugin.Name)
		} else {
			return fmt.Errorf("%s %w", plugin.Name, ErrorPluginNotSupported)
		}
	}
	return nil
}

func (pm *pluginManager) Handle(plugins []config.Plugin, project *config.Project, DstSrcMap map[string]string) error {
	var err error
	for _, plugin := range plugins {
		if pmPlugin, exist := pm.plugins[plugin.Name]; exist {
			zap.Infof("%s %s", plugin.Name, project.Name)
			err = pmPlugin.Handle(project, DstSrcMap)
			if err != nil {
				zap.Errorf("%s %s %v", plugin.Name, project.Name, err)
				return err
			}
			zap.Infof("Done %s %s", plugin.Name, project.Name)
		} else {
			return fmt.Errorf("%s %w", plugin.Name, ErrorPluginNotSupported)
		}
	}
	return nil
}
