package gitlabpush

import (
	"errors"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/banggua/ggtsync/config"
	"gitlab.com/banggua/ggtsync/logger/zap"
)

const (
	// PluginName -
	// TODO:
	PluginName = "gitlabpush"
)

type gitlabpushPlugin struct {
	client *gitlab.Client
}

var (
	defaultPlugin = &gitlabpushPlugin{}

	// Init -
	// TODO:
	Init = defaultPlugin.Init
	// Handle -
	// TODO:
	Handle = defaultPlugin.Handle
)

// Init -
// TSK: [BaseURL Token SSH]
func (gp *gitlabpushPlugin) Init(args []string) error {
	if len(args) != 2 {
		return errors.New("invalid arguments")
	}

	defaultPlugin.client = gitlab.NewClient(nil, args[1])
	return defaultPlugin.client.SetBaseURL(args[0])
}

func (gp *gitlabpushPlugin) Prepare(name, to string) (*gitlab.Group, []*gitlab.Project, error) {
	name = url.PathEscape(name)
	group, resp, err := gp.client.Groups.GetGroup(name)
	handleBadGateway(resp)
	if resp.StatusCode == http.StatusNotFound {
		_, resp, err = gp.client.Groups.CreateGroup(&gitlab.CreateGroupOptions{Name: &name, Visibility: gitlab.Visibility(gitlab.PublicVisibility)})
		handleBadGateway(resp)
	}
	if err != nil {
		zap.Errorf("%s %s %v", name, to, err)
		return nil, nil, err
	}

	var (
		page           = 1
		remoteProjects []*gitlab.Project
	)
	for {
		projs, resp, err := gp.client.Groups.ListGroupProjects(name, &gitlab.ListGroupProjectsOptions{ListOptions: gitlab.ListOptions{Page: page}})
		handleBadGateway(resp)
		if err != nil {
			zap.Errorf("%s %s %v", name, to, err)
			return nil, nil, err
		}
		remoteProjects = append(remoteProjects, projs...)
		if page >= resp.TotalPages {
			break
		}
		page++
	}

	return group, remoteProjects, nil
}

func (gp *gitlabpushPlugin) Compute(remoteProjects []*gitlab.Project, DstSrcMap map[string]string) []operation {
	// none -> scheduled -> started -> finished/failed
	var operations []operation
	for dst, src := range DstSrcMap {
		var isExist = false
		for _, remoteProject := range remoteProjects {
			if filepath.Base(dst) == remoteProject.Path {
				isExist = true
				if remoteProject.ImportStatus == "finished" {
					operations = append(operations, &updateOperation{path: dst, remote: src, project: remoteProject})
				} else if remoteProject.ImportStatus == "failed" {
					operations = append(operations, &retryOperation{path: dst, remote: src, project: remoteProject})
				}
				break
			}
		}
		if !isExist {
			operations = append(operations, &createOperation{path: dst, remote: src})
		}
	}

	for _, remoteProject := range remoteProjects {
		var isExist = false
		for dst := range DstSrcMap {
			if dst == remoteProject.Path {
				isExist = true
				break
			}
		}
		if !isExist {
			operations = append(operations, &deleteOperation{project: remoteProject})
		}
	}

	return operations
}

// Handle -
// TSK:
func (gp *gitlabpushPlugin) Handle(project *config.Project, DstSrcMap map[string]string) error {
	group, remoteProjects, err := gp.Prepare(project.Name, project.To)
	if err != nil {
		zap.Errorf("%s %v", project.Name, err)
		return err
	}

	operations := gp.Compute(remoteProjects, DstSrcMap)
	for _, operation := range operations {
		zap.Infof("%s %#v", project.Name, operation)
		err := operation.Run(gp, group)
		if err != nil {
			zap.Errorf("%s %#v %v", project.Name, operation, err)
			return err
		}
		zap.Infof("Done %s %#v", project.Name, operation)
		time.Sleep(30 * time.Second)
	}

	return nil
}
