package gitlabpush

import (
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/banggua/ggtsync/gitutil"
	"gitlab.com/banggua/ggtsync/logger/zap"
)

const (
	maxQuickPushSize int64 = 50 * 1024 * 1024 // 50MB
	pushRefInterval        = 500
)

func handleBadGateway(resp *gitlab.Response) {
	if resp != nil && resp.StatusCode == http.StatusBadGateway {
		zap.Errorf("%s %s %s", resp.Request.URL.String(), resp.StatusCode, "wait 3 minutes")
		time.Sleep(3 * time.Minute)
	}
}

func dirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

type operation interface {
	Run(plugin *gitlabpushPlugin, group *gitlab.Group) error
}

type createOperation struct {
	path, remote string
}

func (co *createOperation) Run(plugin *gitlabpushPlugin, group *gitlab.Group) error {
	_, resp, err := plugin.client.Projects.CreateProject(&gitlab.CreateProjectOptions{
		Name:                            gitlab.String(filepath.Base(co.path)),
		Path:                            gitlab.String(filepath.Base(co.path)),
		NamespaceID:                     &group.ID,
		IssuesEnabled:                   gitlab.Bool(false),
		MergeRequestsEnabled:            gitlab.Bool(false),
		JobsEnabled:                     gitlab.Bool(false),
		WikiEnabled:                     gitlab.Bool(false),
		SnippetsEnabled:                 gitlab.Bool(false),
		ResolveOutdatedDiffDiscussions:  gitlab.Bool(false),
		ContainerRegistryEnabled:        gitlab.Bool(false),
		SharedRunnersEnabled:            gitlab.Bool(false),
		Visibility:                      gitlab.Visibility(gitlab.PublicVisibility),
		ImportURL:                       gitlab.String(co.remote),
		Mirror:                          gitlab.Bool(true),
		PublicBuilds:                    gitlab.Bool(false),
		LFSEnabled:                      gitlab.Bool(true),
		RequestAccessEnabled:            gitlab.Bool(false),
		PrintingMergeRequestLinkEnabled: gitlab.Bool(false),
	})
	handleBadGateway(resp)
	if err != nil {
		zap.Errorf("createOperation %s %s %v", co.path, co.remote, err)
		return err
	}

	return nil
}

type retryOperation struct {
	path, remote string
	project      *gitlab.Project
}

func (ro *retryOperation) Run(plugin *gitlabpushPlugin, group *gitlab.Group) error {
	resp, err := plugin.client.Projects.DeleteProject(ro.project.ID)
	handleBadGateway(resp)
	if err != nil {
		zap.Errorf("retryOperation %s %s %v", ro.path, ro.remote, err)
		return err
	}

	time.Sleep(3 * time.Minute)
	_, resp, err = plugin.client.Projects.CreateProject(&gitlab.CreateProjectOptions{
		Name:                            gitlab.String(filepath.Base(ro.path)),
		Path:                            gitlab.String(filepath.Base(ro.path)),
		NamespaceID:                     &group.ID,
		IssuesEnabled:                   gitlab.Bool(false),
		MergeRequestsEnabled:            gitlab.Bool(false),
		JobsEnabled:                     gitlab.Bool(false),
		WikiEnabled:                     gitlab.Bool(false),
		SnippetsEnabled:                 gitlab.Bool(false),
		ResolveOutdatedDiffDiscussions:  gitlab.Bool(false),
		ContainerRegistryEnabled:        gitlab.Bool(false),
		SharedRunnersEnabled:            gitlab.Bool(false),
		Visibility:                      gitlab.Visibility(gitlab.PublicVisibility),
		ImportURL:                       gitlab.String(ro.remote),
		Mirror:                          gitlab.Bool(true),
		PublicBuilds:                    gitlab.Bool(false),
		LFSEnabled:                      gitlab.Bool(true),
		RequestAccessEnabled:            gitlab.Bool(false),
		PrintingMergeRequestLinkEnabled: gitlab.Bool(false),
	})
	handleBadGateway(resp)
	if err != nil {
		zap.Errorf("retryOperation %s %s %v", ro.path, ro.remote, err)
		return err
	}

	return nil
}

type deleteOperation struct {
	project *gitlab.Project
}

func (do *deleteOperation) Run(plugin *gitlabpushPlugin, group *gitlab.Group) error {
	resp, err := plugin.client.Projects.DeleteProject(do.project.ID)
	handleBadGateway(resp)
	if err != nil {
		zap.Errorf("deleteOperation %s %s %v", do.project.HTTPURLToRepo, err)
		return err
	}
	return nil
}

type updateOperation struct {
	path    string
	remote  string
	project *gitlab.Project
}

func (uo *updateOperation) Run(plugin *gitlabpushPlugin, group *gitlab.Group) (err error) {
	defer func() {
		if err != nil && strings.Contains(err.Error(), "fatal: pack exceeds maximum allowed size") {
			resp, derr := plugin.client.Projects.DeleteProject(uo.project.ID)
			handleBadGateway(resp)
			if derr != nil {
				zap.Errorf("updateOperation Retry %s %s %v", uo.path, uo.remote, derr)
				err = derr
			}

			time.Sleep(3 * time.Minute)
			_, resp, derr = plugin.client.Projects.CreateProject(&gitlab.CreateProjectOptions{
				Name:                            gitlab.String(filepath.Base(uo.path)),
				Path:                            gitlab.String(filepath.Base(uo.path)),
				NamespaceID:                     &group.ID,
				IssuesEnabled:                   gitlab.Bool(false),
				MergeRequestsEnabled:            gitlab.Bool(false),
				JobsEnabled:                     gitlab.Bool(false),
				WikiEnabled:                     gitlab.Bool(false),
				SnippetsEnabled:                 gitlab.Bool(false),
				ResolveOutdatedDiffDiscussions:  gitlab.Bool(false),
				ContainerRegistryEnabled:        gitlab.Bool(false),
				SharedRunnersEnabled:            gitlab.Bool(false),
				Visibility:                      gitlab.Visibility(gitlab.PublicVisibility),
				ImportURL:                       gitlab.String(uo.remote),
				Mirror:                          gitlab.Bool(true),
				PublicBuilds:                    gitlab.Bool(false),
				LFSEnabled:                      gitlab.Bool(true),
				RequestAccessEnabled:            gitlab.Bool(false),
				PrintingMergeRequestLinkEnabled: gitlab.Bool(false),
			})
			handleBadGateway(resp)
			if derr != nil {
				zap.Errorf("updateOperation Retry %s %s %v", uo.path, uo.remote, derr)
				err = derr
			}
		}
	}()

	resp, err := plugin.client.ProtectedBranches.UnprotectRepositoryBranches(uo.project.ID, "master")
	handleBadGateway(resp)
	if resp != nil && resp.StatusCode != http.StatusNotFound {
		err = nil
	}

	shortBranches, err := gitutil.GetShortBranches(uo.path)
	if err != nil {
		zap.Errorf("updateOperation %s %s %v", uo.path, uo.project.HTTPURLToRepo, err)
		return err
	}
	shortTags, err := gitutil.GetShortTags(uo.path)
	if err != nil {
		zap.Errorf("updateOperation %s %s %v", uo.path, uo.project.HTTPURLToRepo, err)
		return err
	}

	var refs []string
	branches, err := gitutil.GetBranches(uo.path)
	if err != nil {
		zap.Errorf("updateOperation %s %s %v", uo.path, uo.project.HTTPURLToRepo, err)
		return err
	}
	tags, err := gitutil.GetTags(uo.path)
	if err != nil {
		zap.Errorf("updateOperation %s %s %v", uo.path, uo.project.HTTPURLToRepo, err)
		return err
	}
	refs = append(refs, branches...)
	refs = append(refs, tags...)

	var localRefHashMap = make(map[string]string)
	for _, ref := range refs {
		hash, err := gitutil.GetRefHash(uo.path, ref)
		if err != nil {
			zap.Errorf("updateOperation %s %s %s %v", uo.path, uo.project.HTTPURLToRepo, ref, err)
			return err
		}
		localRefHashMap[ref] = hash
	}

	var remoteRefHashMap = make(map[string]string)
	for _, branch := range shortBranches {
		remoteBranch, resp, err := plugin.client.Branches.GetBranch(uo.project.ID, url.PathEscape(branch))
		handleBadGateway(resp)
		if (remoteBranch == nil) || (resp != nil && resp.StatusCode == http.StatusNotFound) {
			err = nil
			remoteRefHashMap["refs/heads/"+branch] = ""
			continue
		}
		if err != nil {
			zap.Errorf("updateOperation %s %s %s %v", uo.path, uo.project.HTTPURLToRepo, branch, err)
			return err
		}
		if _, exist := localRefHashMap["refs/heads/"+branch]; exist {
			remoteRefHashMap["refs/heads/"+branch] = remoteBranch.Commit.ID
		} else {
			resp, err := plugin.client.Branches.DeleteBranch(uo.project.ID, url.PathEscape(branch))
			handleBadGateway(resp)
			if err != nil {
				zap.Errorf("updateOperation %s %s %s %v", uo.path, uo.project.HTTPURLToRepo, branch, err)
				return err
			}
		}
	}
	for _, tag := range shortTags {
		remoteTag, resp, err := plugin.client.Tags.GetTag(uo.project.ID, url.PathEscape(tag))
		handleBadGateway(resp)
		if (remoteTag == nil) || (resp != nil && resp.StatusCode == http.StatusNotFound) {
			err = nil
			remoteRefHashMap["refs/tags/"+tag] = ""
			continue
		}
		if err != nil {
			zap.Errorf("updateOperation %s %s %s %v", uo.path, uo.project.HTTPURLToRepo, tag, err)
			return err
		}
		if _, exist := localRefHashMap["refs/tags/"+tag]; exist {
			remoteRefHashMap["refs/tags/"+tag] = remoteTag.Commit.ID
		} else {
			resp, err := plugin.client.Tags.DeleteTag(uo.project.ID, url.PathEscape(tag))
			handleBadGateway(resp)
			if err != nil {
				zap.Errorf("updateOperation %s %s %s %v", uo.path, uo.project.HTTPURLToRepo, tag, err)
				return err
			}
		}
	}

	for _, ref := range refs {
		if localRefHashMap[ref] == remoteRefHashMap[ref] {
			zap.Infof("updateOperation Skip %s %s", uo.path, ref)
			continue
		}
		zap.Infof("updateOperation Quick PushRef %s %s %s %s", uo.path, uo.project.SSHURLToRepo, localRefHashMap[ref], ref)
		err := gitutil.PushRef(uo.path, uo.project.SSHURLToRepo, localRefHashMap[ref], ref, "--force")
		if err != nil {
			zap.Errorf("updateOperation Quick PushRef %s %s %s %s %v", uo.path, uo.project.SSHURLToRepo, localRefHashMap[ref], ref, err)
		} else {
			zap.Infof("Done updateOperation Quick PushRef %s %s %s %s", uo.path, uo.project.SSHURLToRepo, localRefHashMap[ref], ref)
			continue
		}

		var (
			index    = 0
			interval = pushRefInterval
			match    = false
		)
		hashs, err := gitutil.GetRefHashs(uo.path, ref)
		if err != nil {
			zap.Errorf("updateOperation %s %s %v", uo.path, ref, err)
			return err
		}
		for _, hash := range hashs {
			if hash == remoteRefHashMap[ref] {
				match = true
				break
			}
			index++
		}
		if !match {
			index = 0
		}
		for ; interval > 0; interval /= 2 {
			if index > len(hashs)-1 {
				index = len(hashs) - 1
			}
			zap.Infof("updateOperation Slow PushRef %s %s %s %s", uo.path, uo.project.SSHURLToRepo, hashs[index], ref)
			err = gitutil.PushRef(uo.path, uo.project.SSHURLToRepo, hashs[index], ref, "--force")
			if err != nil {
				continue
			}
			zap.Infof("Done updateOperation Slow PushRef  %s %s %s %s", uo.path, uo.project.SSHURLToRepo, hashs[index], ref)
			index += interval
			interval = pushRefInterval
		}
		if err != nil {
			zap.Errorf("updateOperation Slow PushRef %s %s %s %s %v", uo.path, uo.project.SSHURLToRepo, hashs[index], ref, err)
			return err
		}
	}

	return nil
}
