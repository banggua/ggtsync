package handler

import (
	"sync"
	"time"

	"gitlab.com/banggua/ggtsync/config"
	"gitlab.com/banggua/ggtsync/handler/jiri"
	"gitlab.com/banggua/ggtsync/logger/zap"
	"gitlab.com/banggua/ggtsync/plugin"
)

// Handler -
// TODO:
type Handler interface {
	Handle(*config.Project) (map[string]string, error)
	Close(*config.Project) error
}

type manager struct {
	locker  *sync.Mutex
	doneChs map[string]chan struct{}
	closeCh chan struct{}
}

var (
	defaultManager = &manager{
		locker:  &sync.Mutex{},
		doneChs: make(map[string]chan struct{}),
		closeCh: make(chan struct{}),
	}
)

// TSK:
func handle(handleFn func(*config.Project) (map[string]string, error), closeFn func(*config.Project) error, project *config.Project, doneCh <-chan struct{}, closeCh <-chan struct{}) {
	zap.Infof("%s", project.Name)
	var (
		retryTimes, maxTimes = 0, 30
		before, after        = project.GetPlugins()
	)
	if err := plugin.Init(before); err != nil {
		zap.Errorf("%s %v", project.Name, err)
	}
	if err := plugin.Init(after); err != nil {
		zap.Errorf("%s %v", project.Name, err)
	}
	for {
		for ; retryTimes < maxTimes; retryTimes++ {
			err := plugin.Handle(before, project, nil)
			if err != nil {
				zap.Errorf("%s %v", project.Name, err)
				continue
			}
			zap.Infof("%s", project.Name)
			DstSrcMap, err := handleFn(project)
			if err != nil {
				zap.Errorf("%s %v", project.Name, err)
				continue
			}
			zap.Infof("Done %s", project.Name)
			err = plugin.Handle(after, project, DstSrcMap)
			if err != nil {
				zap.Errorf("%s %v", project.Name, err)
			}
			if err == nil {
				break
			}
		}
		retryTimes = 0
		select {
		case <-doneCh:
			err := closeFn(project)
			if err != nil {
				zap.Errorf("%s %v", project.Name, err)
			}
			return
		case <-closeCh:
			err := closeFn(project)
			if err != nil {
				zap.Errorf("%s %v", project.Name, err)
			}
			return
		case <-time.After(time.Duration(project.Interval * time.Hour.Nanoseconds())):
		}
	}
}

func (m *manager) Handle(project *config.Project) {
	m.locker.Lock()
	defer m.locker.Unlock()

	doneCh := make(chan struct{})
	m.doneChs[project.Name] = doneCh

	switch project.Type {
	case config.ProjectTypeJiri:
		go handle(jiri.Handle, jiri.Close, project, doneCh, m.closeCh)
	}
}

func (m *manager) Close(project *config.Project) {
	m.locker.Lock()
	defer m.locker.Unlock()

	close(m.doneChs[project.Name])
	delete(m.doneChs, project.Name)
}

func (m *manager) Wait() {
	<-m.closeCh
}

func (m *manager) Shutdown() {
	close(m.closeCh)
}
