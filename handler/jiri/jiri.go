package jiri

import (
	"errors"
	"flag"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"fuchsia.googlesource.com/jiri"
	"fuchsia.googlesource.com/jiri/cmdline"
	"fuchsia.googlesource.com/jiri/project"

	"gitlab.com/banggua/ggtsync/config"
	"gitlab.com/banggua/ggtsync/logger/zap"
)

type jiriClient struct {
	locker *sync.Mutex
	Xs     map[string]*jiri.X
}

var (
	defaultJiriClient = &jiriClient{
		locker: &sync.Mutex{},
		Xs:     make(map[string]*jiri.X),
	}

	// Handle -
	// TODO:
	Handle = defaultJiriClient.Handle
	// Close -
	// TODO:
	Close = defaultJiriClient.Close
)

const (
	jiriSource      = ".jiri_source"
	jiriDestination = ".jiri_destin"
)

func (jc *jiriClient) Init(name, to string) (*jiri.X, error) {
	jiriSourceDir := filepath.Join(to, jiriSource)
	if _, err := os.Stat(jiriSourceDir); err != nil {
		if !os.IsNotExist(err) {
			zap.Errorf("%s %s %v", name, to, err)
			return nil, err
		}
		if err := os.MkdirAll(jiriSourceDir, 0755); err != nil {
			zap.Errorf("%s %s %v", name, to, err)
			return nil, err
		}
	}

	jiriRootDir := filepath.Join(jiriSourceDir, jiri.RootMetaDir)
	if _, err := os.Stat(jiriRootDir); err != nil {
		if !os.IsNotExist(err) {
			zap.Errorf("%s %s %v", name, to, err)
			return nil, err
		}
		if err := os.Mkdir(jiriRootDir, 0755); err != nil {
			zap.Errorf("%s %s %v", name, to, err)
			return nil, err
		}
	}

	config := &jiri.Config{}
	configPath := filepath.Join(jiriRootDir, jiri.ConfigFile)
	if _, err := os.Stat(configPath); err == nil {
		config, err = jiri.ConfigFromFile(configPath)
		if err != nil {
			zap.Errorf("%s %s %v", name, to, err)
			return nil, err
		}
	} else if !os.IsNotExist(err) {
		zap.Errorf("%s %s %v", name, to, err)
		return nil, err
	}

	if err := config.Write(configPath); err != nil {
		zap.Errorf("%s %s %v", name, to, err)
		return nil, err
	}
	// TODO(phosek): also create an empty manifest

	if err := flag.Set("root", jiriSourceDir); err != nil {
		zap.Errorf("%s %s %v", name, to, err)
		return nil, err
	}

	jirix, err := jiri.NewX(cmdline.EnvFromOS())
	if err != nil {
		zap.Errorf("%s %s %v", name, to, err)
		return nil, err
	}

	jc.locker.Lock()
	defer jc.locker.Unlock()
	jc.Xs[name] = jirix

	return jirix, nil
}

// ErrorInvalidFrom -
// TSK:
var ErrorInvalidFrom = errors.New("from field is invalid")

func getArgs(from string) (string, string, string, error) {
	args := strings.Split(from, " ")
	if len(args) != 3 {
		zap.Errorf("%s %v", from, ErrorInvalidFrom)
		return "", "", "", ErrorInvalidFrom
	}
	return args[0], args[1], args[2], nil

}

func isFile(file string) (bool, error) {
	fileInfo, err := os.Stat(file)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		zap.Errorf("%s %v", file, err)
		return false, err
	}
	return !fileInfo.IsDir(), nil
}

func (jc *jiriClient) Import(jirix *jiri.X, from string) error {
	projectName, manifestName, remoteURL, err := getArgs(from)
	if err != nil {
		zap.Errorf("%s %v", from, err)
		return err
	}

	var manifest *project.Manifest
	isExist, err := isFile(jirix.JiriManifestFile())
	if err != nil {
		zap.Errorf("%s %v", from, err)
		return err
	}

	if isExist {
		m, err := project.ManifestFromFile(jirix, jirix.JiriManifestFile())
		if err != nil {
			zap.Errorf("%s %v", from, err)
			return err
		}
		manifest = m
	}

	if manifest == nil {
		manifest = &project.Manifest{}
	}

	for _, imp := range manifest.Imports {
		if imp.Manifest == manifestName && imp.Name == projectName && imp.Remote == remoteURL {
			return nil
		}
	}

	manifest.Imports = append(manifest.Imports, project.Import{
		Manifest:     manifestName,
		Name:         projectName,
		Remote:       remoteURL,
		RemoteBranch: "",
		Revision:     "",
		Root:         "",
	})

	return manifest.ToFile(jirix, jirix.JiriManifestFile())
}

func handleRemoteProjects(remoteProjects project.Projects, to string) error {
	for remoteKey, remoteProject := range remoteProjects {
		rURL, err := url.Parse(remoteProject.Remote)
		if err != nil {
			zap.Errorf("%s %v", remoteProject.Remote, err)
			return err
		}
		remoteProject.Path = filepath.Join(to, rURL.Host+strings.ReplaceAll(strings.TrimSuffix(rURL.Path, ".git"), "/", "-"))
		remoteProject.Path = strings.TrimSuffix(remoteProject.Path, "-")
		remoteProjects[remoteKey] = remoteProject
	}

	return nil
}

func getLocalProjects(to string) ([]string, error) {
	return filepath.Glob(filepath.Join(to, "[^.]*"))
}

func updateProjects(localProjects []string, remoteProjects project.Projects, hooks project.Hooks, pkgs project.Packages) (map[string]string, error) {
	var (
		operations = make(map[string]operation)
		DstSrcMap  = make(map[string]string)
	)
	for _, remoteProject := range remoteProjects {
		var isExist = false
		for _, localProject := range localProjects {
			if localProject == remoteProject.Path {
				isExist = true
				if _, exist := operations[remoteProject.Path]; !exist {
					operations[remoteProject.Path] = &updateOperation{path: remoteProject.Path, remote: remoteProject.Remote}
					DstSrcMap[remoteProject.Path] = remoteProject.Remote
				}
				break
			}
		}
		if !isExist {
			if _, exist := operations[remoteProject.Path]; !exist {
				operations[remoteProject.Path] = &createOperation{path: remoteProject.Path, remote: remoteProject.Remote}
				DstSrcMap[remoteProject.Path] = remoteProject.Remote
			}
		}
	}

	for _, localProject := range localProjects {
		var isExist = false
		for _, remoteProject := range remoteProjects {
			if localProject == remoteProject.Path {
				isExist = true
				break
			}
		}
		if !isExist {
			if _, exist := operations[localProject]; !exist {
				operations[localProject] = &deleteOperation{path: localProject}
			}
		}
	}

	for _, operation := range operations {
		zap.Infof("%#v", operation)
		err := operation.Run()
		if err != nil {
			zap.Errorf("%#v %v", operation, err)
			return nil, err
		}
		zap.Infof("Done %#v", operation)
	}
	return DstSrcMap, nil
}

func (jc *jiriClient) Update(jirix *jiri.X, to string) (map[string]string, error) {
	// Find all local projects.
	loadProjects, err := project.LocalProjects(jirix, project.FullScan)
	if err != nil {
		zap.Errorf("%v", err)
		return nil, err
	}

	localProjects, err := getLocalProjects(to)
	if err != nil {
		zap.Errorf("%v", err)
		return nil, err
	}

	// Determine the set of remote projects and match them up with the locals.
	remoteProjects, hooks, pkgs, err := project.LoadUpdatedManifest(jirix, loadProjects, false)
	project.MatchLocalWithRemote(loadProjects, remoteProjects)
	if err != nil {
		zap.Errorf("%v", err)
		return nil, err
	}

	err = handleRemoteProjects(remoteProjects, to)
	if err != nil {
		zap.Errorf("%v", err)
		return nil, err
	}

	zap.Infof("number loadProjects %d, localProjects %d,remoteProjects %d, hooks %d, pkgs %d", len(loadProjects), len(localProjects), len(remoteProjects), len(hooks), len(pkgs))

	return updateProjects(localProjects, remoteProjects, hooks, pkgs)
}

// Handle -
// TODO:
func (jc *jiriClient) Handle(project *config.Project) (map[string]string, error) {
	jirix, err := jc.Init(project.Name, project.To)
	if err != nil {
		zap.Errorf("%v", err)
		return nil, err
	}

	err = jc.Import(jirix, project.From)
	if err != nil {
		zap.Errorf("%v", err)
		return nil, err
	}

	return jc.Update(jirix, project.To)
}

// Close -
// TODO:
func (jc *jiriClient) Close(project *config.Project) error {
	jc.locker.Lock()
	defer jc.locker.Unlock()
	jc.Xs[project.Name].RunCleanup()
	delete(jc.Xs, project.Name)

	return nil
}
