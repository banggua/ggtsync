package jiri

import (
	"os"
	"path/filepath"

	"gitlab.com/banggua/ggtsync/gitutil"
)

type operation interface {
	Run() error
}

type createOperation struct {
	path   string
	remote string
}

func (co *createOperation) Run() error {
	return gitutil.CloneBare(filepath.Dir(co.path), co.remote, filepath.Base(co.path))
}

type deleteOperation struct {
	path string
}

func (do *deleteOperation) Run() error {
	return os.RemoveAll(do.path)
}

type updateOperation struct {
	path   string
	remote string
}

func (uo *updateOperation) Run() error {
	return gitutil.FetchAll(uo.path)
}
