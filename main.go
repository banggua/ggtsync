package main

import (
	"flag"
	"os"

	"gitlab.com/banggua/ggtsync/command"
)

var (
	flagSet = flag.NewFlagSet("ggtsync", flag.ExitOnError)

	helpFlag    bool
	versionFlag bool
	testFlag    bool
	signalFlag  string
	configFlag  string
)

func init() {
	flagSet.BoolVar(&helpFlag, "h", false, "this help")
	flagSet.BoolVar(&versionFlag, "v", false, "show version and exit")
	flagSet.BoolVar(&testFlag, "t", false, "test configuration and exit")
	flagSet.StringVar(&signalFlag, "s", "start", "send signal to a master process: start, quit, reload")
	flagSet.StringVar(&configFlag, "c", "config.yaml", "set configuration file")
	flagSet.Usage = command.Help
	flagSet.Parse(os.Args[1:])
}

func main() {
	if helpFlag {
		command.Help()
	}

	if versionFlag {
		command.Version()
	}

	if testFlag {
		command.Test(configFlag)
	}

	command.Signal(signalFlag, configFlag)
}
