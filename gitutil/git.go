package gitutil

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
)

type GitError struct {
	Root        string
	Args        []string
	Output      string
	ErrorOutput string
	err         error
}

func Error(output, errorOutput string, err error, root string, args ...string) GitError {
	return GitError{
		Root:        root,
		Args:        args,
		Output:      output,
		ErrorOutput: errorOutput,
		err:         err,
	}
}

func (ge GitError) Error() string {
	result := fmt.Sprintf("(%s)", ge.Root)
	result = "'git "
	result += strings.Join(ge.Args, " ")
	result += "' failed:\n"
	result += "stdout:\n"
	result += ge.Output + "\n"
	result += "stderr:\n"
	result += ge.ErrorOutput
	result += "\ncommand fail error: " + ge.err.Error()
	return result
}

func Run(root string, args ...string) error {
	return run(root, args...)
}

func run(root string, args ...string) error {
	var stdout, stderr bytes.Buffer
	if err := runGit(&stdout, &stderr, root, args...); err != nil {
		return err
	}
	return nil
}

func RunOutputOne(root string, args ...string) (string, error) {
	return runOutputOne(root, args...)
}

func runOutputOne(root string, args ...string) (string, error) {
	var stdout, stderr bytes.Buffer
	if err := runGit(&stdout, &stderr, root, args...); err != nil {
		return "", err
	}
	return strings.ReplaceAll(strings.TrimSuffix(stdout.String(), "\n"), "\"", ""), nil
}

func trimOutput(o string) []string {
	output := strings.TrimSpace(o)
	if len(output) == 0 {
		return nil
	}
	return strings.Split(output, "\n")
}

func RunOutput(root string, args ...string) ([]string, error) {
	return runOutput(root, args...)
}

func runOutput(root string, args ...string) ([]string, error) {
	var stdout, stderr bytes.Buffer
	if err := runGit(&stdout, &stderr, root, args...); err != nil {
		return nil, err
	}
	return trimOutput(strings.ReplaceAll(stdout.String(), "\"", "")), nil
}

func RunInteractive(root string, args ...string) error {
	return runInteractive(root, args...)
}

func runInteractive(root string, args ...string) error {
	var stderr bytes.Buffer
	// In order for the editing to work correctly with
	// terminal-based editors, notably "vim", use os.Stdout.
	if err := runGit(os.Stdout, &stderr, root, args...); err != nil {
		return err
	}
	return nil
}

func RunGit(stdout, stderr io.Writer, root string, args ...string) error {
	return runGit(stdout, stderr, root, args...)
}

func runGit(stdout, stderr io.Writer, root string, args ...string) error {
	var outbuf bytes.Buffer
	var errbuf bytes.Buffer
	command := exec.Command("git", args...)
	command.Dir = root
	command.Stdin = os.Stdin
	command.Stdout = io.MultiWriter(stdout, &outbuf)
	command.Stderr = io.MultiWriter(stderr, &errbuf)
	err := command.Run()
	if err != nil {
		return Error(outbuf.String(), errbuf.String(), err, root, args...)
	}
	return nil
}
