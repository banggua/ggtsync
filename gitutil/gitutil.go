package gitutil

import (
	"fmt"
)

func CloneBare(root, remote string, args ...string) error {
	return Run(root, append([]string{"clone", "--bare", remote}, args...)...)
}

func FetchAll(root string, args ...string) error {
	return Run(root, append([]string{"fetch", "--all"}, args...)...)
}

func GetBranches(root string, args ...string) ([]string, error) {
	return RunOutput(root, append([]string{"branch", "--list", `--format="%(refname)"`}, args...)...)
}

func GetShortBranches(root string, args ...string) ([]string, error) {
	return RunOutput(root, append([]string{"branch", "--list", `--format="%(refname:short)"`}, args...)...)
}

func GetTags(root string, args ...string) ([]string, error) {
	return RunOutput(root, append([]string{"tag", "--list", `--format="%(refname)"`}, args...)...)
}

func GetShortTags(root string, args ...string) ([]string, error) {
	return RunOutput(root, append([]string{"tag", "--list", `--format="%(refname:short)"`}, args...)...)
}

func GetAllHashs(root string, args ...string) ([]string, error) {
	return RunOutput(root, append([]string{"log", "--reverse", "--all", `--format="%H"`}, args...)...)
}

func GetRefHash(root, ref string, args ...string) (string, error) {
	out, err := RunOutputOne(root, append([]string{"log", ref, "-n1", `--format="%H"`}, args...)...)
	if err != nil {
		out, err = RunOutputOne(root, append([]string{"log", "--", ref, "-n1", `--format="%H"`}, args...)...)
	}
	return out, nil
}

func GetRefHashs(root, ref string, args ...string) ([]string, error) {
	out, err := RunOutput(root, append([]string{"log", ref, "--reverse", `--format=format:"%H"`}, args...)...)
	if err != nil {
		out, err = RunOutput(root, append([]string{"log", "--", ref, "--reverse", `--format=format:"%H"`}, args...)...)
	}
	return out, err
}

func PushMirror(root, remote string, args ...string) error {
	return Run(root, append([]string{"push", "--mirror", remote}, args...)...)
}

func PushRef(root, remote, hash, ref string, args ...string) error {
	return Run(root, append([]string{"push", remote, fmt.Sprintf("%s:%s", hash, ref)}, args...)...)
}
