package zap

import (
	"io"
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	// Debug is a global method for showing debuging messages.
	Debug func(...interface{})

	// Info is a global method for showing debuging messages.
	Info func(...interface{})

	// Warn is a global method for showing debuging messages.
	Warn func(...interface{})

	// Error is a global method for showing debuging messages.
	Error func(...interface{})

	// Fatal is a global method for showing debuging messages.
	Fatal func(...interface{})

	// Debugf is a global method for showing debuging messages.
	Debugf func(string, ...interface{})

	// Infof is a global method for showing debuging messages.
	Infof func(string, ...interface{})

	// Warnf is a global method for showing debuging messages.
	Warnf func(string, ...interface{})

	// Errorf is a global method for showing debuging messages.
	Errorf func(string, ...interface{})

	// Fatalf is a global method for showing debuging messages.
	Fatalf func(string, ...interface{})
)

func init() {
	lg, err := zap.NewProduction(zap.AddCaller(), zap.AddCallerSkip(1))
	if err != nil {
		panic(err)
	}
	defaultLogger := &zapLogger{lg.Sugar()}

	Debug = defaultLogger.Debug
	Info = defaultLogger.Info
	Warn = defaultLogger.Warn
	Error = defaultLogger.Error
	Fatal = defaultLogger.Fatal

	Debugf = defaultLogger.Debugf
	Infof = defaultLogger.Infof
	Warnf = defaultLogger.Warnf
	Errorf = defaultLogger.Errorf
	Fatalf = defaultLogger.Fatalf
}

// LogToFile -
// TODO:
func LogToFile() {
	encoder := zapcore.NewJSONEncoder(zapcore.EncoderConfig{
		TimeKey:       "ts",
		LevelKey:      "level",
		NameKey:       "logger",
		CallerKey:     "caller",
		MessageKey:    "msg",
		StacktraceKey: "stacktrace",
		LineEnding:    zapcore.DefaultLineEnding,
		EncodeLevel:   zapcore.CapitalLevelEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.In(time.FixedZone("CST", 8*3600)).Format(time.RFC3339Nano))
		},
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	})

	infoWriter := getWriter("info.log")
	errorWriter := getWriter("error.log")

	core := zapcore.NewTee(
		zapcore.NewCore(encoder, zapcore.AddSync(infoWriter), zap.NewAtomicLevelAt(zap.InfoLevel)),
		zapcore.NewCore(encoder, zapcore.AddSync(errorWriter), zap.NewAtomicLevelAt(zap.ErrorLevel)),
	)

	lg := zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1))
	defaultLogger := &zapLogger{lg.Sugar()}

	Debug = defaultLogger.Debug
	Info = defaultLogger.Info
	Warn = defaultLogger.Warn
	Error = defaultLogger.Error
	Fatal = defaultLogger.Fatal

	Debugf = defaultLogger.Debugf
	Infof = defaultLogger.Infof
	Warnf = defaultLogger.Warnf
	Errorf = defaultLogger.Errorf
	Fatalf = defaultLogger.Fatalf
}

func getWriter(filename string) io.Writer {
	hook, err := rotatelogs.New(
		filename+".%Y%m%d",
		rotatelogs.WithLinkName(filename),
		rotatelogs.WithMaxAge(30*24*time.Hour),
		rotatelogs.WithRotationTime(24*time.Hour),
	)

	if err != nil {
		panic(err)
	}
	return hook
}
