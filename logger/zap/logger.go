package zap

import (
	"go.uber.org/zap"

	"gitlab.com/banggua/ggtsync/logger"
)

var (
	_ logger.Logger = &zapLogger{}
)

type zapLogger struct {
	lg *zap.SugaredLogger
}

// Debug - Logger implementation.
func (l *zapLogger) Debug(args ...interface{}) {
	l.lg.Debug(args...)
}

// Info - Logger implementation.
func (l *zapLogger) Info(args ...interface{}) {
	l.lg.Info(args...)
}

// Warn - Logger implementation.
func (l *zapLogger) Warn(args ...interface{}) {
	l.lg.Warn(args...)
}

// Error - Logger implementation.
func (l *zapLogger) Error(args ...interface{}) {
	l.lg.Error(args...)
}

// Fatal - Logger implementation.
func (l *zapLogger) Fatal(args ...interface{}) {
	l.lg.Fatal(args...)
}

// Debugf - Logger implementation.
func (l *zapLogger) Debugf(format string, args ...interface{}) {
	l.lg.Debugf(format, args...)
}

// Infof - Logger implementation.
func (l *zapLogger) Infof(format string, args ...interface{}) {
	l.lg.Infof(format, args...)
}

// Warnf - Logger implementation.
func (l *zapLogger) Warnf(format string, args ...interface{}) {
	l.lg.Warnf(format, args...)
}

// Errorf - Logger implementation.
func (l *zapLogger) Errorf(format string, args ...interface{}) {
	l.lg.Errorf(format, args...)
}

// Fatalf - Logger implementation.
func (l *zapLogger) Fatalf(format string, args ...interface{}) {
	l.lg.Fatalf(format, args...)
}
