module gitlab.com/banggua/ggtsync

go 1.13

require (
	fuchsia.googlesource.com/jiri v0.0.0
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-playground/validator/v10 v10.1.0
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/jonboulle/clockwork v0.1.0 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.3.0+incompatible
	github.com/lestrrat-go/strftime v1.0.1 // indirect
	github.com/tebeka/strftime v0.1.3 // indirect
	github.com/xanzy/go-gitlab v0.25.0
	go.uber.org/zap v1.13.0
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
	golang.org/x/sys v0.0.0-20200212091648-12a6c2dcc1e4 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.8
)

replace fuchsia.googlesource.com/jiri v0.0.0 => gitlab.com/fuchsia-cn/fuchsia/jiri v0.0.0-20200121172525-59478f6e3bff
