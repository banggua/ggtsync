package config

import (
	"errors"
	"fmt"
	"testing"
)

func Test_client_Load(t *testing.T) {
	type TestCase struct {
		name     string
		filename string
		wantErr  bool
	}
	tests := []TestCase{
		TestCase{
			name:     "good",
			filename: "test/good/config.yaml",
			wantErr:  false,
		},
		TestCase{
			name:     "bad ErrorConfigPathToInvalid",
			filename: "test/bad/ErrorConfigPathToInvalid/config.yaml",
			wantErr:  true,
		},
		TestCase{
			name:     "bad ErrorConfigProjectExist",
			filename: "test/bad/ErrorConfigProjectExist/config.yaml",
			wantErr:  true,
		},
		TestCase{
			name:     "bad ErrorOrderInvalid",
			filename: "test/bad/ErrorOrderInvalid/config.yaml",
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defaultClient.renew()
			err := defaultClient.Load(tt.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("Load() error = %v, wantErr %v", err, tt.wantErr)
			}
			fmt.Printf("%#v", errors.Unwrap(err))
		})
	}
}
