package config

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"sync"

	"github.com/fsnotify/fsnotify"

	"gitlab.com/banggua/ggtsync/logger/zap"
)

var (
	once            = &sync.Once{}
	defaultWatcher  *fsnotify.Watcher
	defaultFileName = ".ggtsync"
	ggtsyncPid      = strconv.Itoa(os.Getpid())
)

func init() {
	wtcher, err := fsnotify.NewWatcher()
	if err != nil {
		zap.Fatal(err)
	}

	home, err := os.UserHomeDir()
	if err != nil {
		zap.Fatal(err)
	}

	defaultWatcher = wtcher
	defaultFileName = filepath.Join(home, defaultFileName)
}

func watching() {
	writepid()
	defaultWatcher.Add(defaultFileName)

	for {
		select {
		case event, ok := <-defaultWatcher.Events:
			zap.Infof("%t %v", ok, event)
			if !ok {
				return
			}
			defaultWatcher.Remove(defaultFileName)
			writepid()
			defaultWatcher.Add(defaultFileName)
		case err, ok := <-defaultWatcher.Errors:
			zap.Infof("%t %v", ok, err)
			if !ok {
				return
			}
			zap.Errorf("%v", err)
		}
	}
}

func shutdown() error {
	err := defaultWatcher.Close()
	if err != nil {
		zap.Errorf("%v", err)
		return err
	}
	return os.Remove(defaultFileName)
}

func readpid() (int, error) {
	file, err := os.Open(defaultFileName)
	if err != nil && !os.IsNotExist(err) {
		zap.Errorf("%v", err)
		return 0, err
	}
	if err != nil && os.IsNotExist(err) {
		return 0, ErrorConfigNotRunning
	}
	defer file.Close()

	body, err := ioutil.ReadAll(file)
	if err != nil {
		zap.Errorf("%v", err)
		return 0, err
	}

	return strconv.Atoi(string(body))
}

func writepid() error {
	file, err := os.OpenFile(defaultFileName, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		zap.Errorf("%v", err)
		return err
	}
	defer file.Close()

	_, err = file.WriteString(ggtsyncPid)
	return err
}
