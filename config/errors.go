package config

import (
	"errors"
)

var (
	// ErrorConfigPathToInvalid -
	// TODO:
	ErrorConfigPathToInvalid = errors.New("directory path is bad")
	// ErrorConfigProjectExist -
	// TODO:
	ErrorConfigProjectExist = errors.New("project already exists")
	// ErrorOrderInvalid -
	// TODO:
	ErrorOrderInvalid = errors.New("order is invalid")
	// ErrorConfigNotRunning -
	// TODO:
	ErrorConfigNotRunning = errors.New("not running")
)
