package config

// Fragment -
// TODO:
type Fragment struct {
	Includes []string  `yaml:"includes,omitempty"`
	Projects []Project `yaml:"projects,omitempty" validate:"dive,required"`
}

// Project -
// TODO:
type Project struct {
	Name     string   `yaml:"name" validate:"required"`
	Order    int      `yaml:"order" validate:"required_with=Plugins"`
	Plugins  []Plugin `yaml:"plugins,omitempty" validate:"omitempty"`
	before   []Plugin
	after    []Plugin
	Type     ProjectType `yaml:"type" validate:"required,oneof=jiri git"`
	From     string      `yaml:"from" validate:"required"`
	To       string      `yaml:"to" validate:"required"`
	Interval int64       `yaml:"interval,omitempty" validate:"min=1"`
}

// GetPlugins -
// TODO:
func (p *Project) GetPlugins() ([]Plugin, []Plugin) {
	return p.before, p.after
}

// ProjectType -
// TODO:
type ProjectType string

var (
	// ProjectTypeJiri -
	// TODO:
	ProjectTypeJiri ProjectType = "jiri"
	// ProjectTypeGit -
	// TODO:
	ProjectTypeGit ProjectType = "git"
)

// Plugin -
// TODO:
type Plugin struct {
	Name  string   `yaml:"name" validate:"required"`
	Order int      `yaml:"order" validate:"required"`
	Args  []string `yaml:"args,omitempty"`
}
