package command

import (
	"fmt"
	"os"
)

const (
	major = 0
	minor = 1
	patch = 0
)

func version() string {
	return fmt.Sprintf(`ggtsync version: ggtsync/%d.%d.%d
`, major, minor, patch)
}

// Version -
// TODO:
func Version() {
	fmt.Fprintf(os.Stdout, "%s", version())
	os.Exit(0)
}
