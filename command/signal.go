package command

import (
	"errors"
	"fmt"
	"os"
	"syscall"

	"gitlab.com/banggua/ggtsync/config"
	"gitlab.com/banggua/ggtsync/handler"
	"gitlab.com/banggua/ggtsync/logger/zap"
)

const (
	configStartSuccess = `ggtsync: successfully start
`
	configStartFailure = `ggtsync: %v
ggtsync: fail to start
`
)

func start(filename string) {
	zap.LogToFile()

	_, err := config.GetPid()
	if err == nil {
		fmt.Fprintf(os.Stderr, configStartFailure, errors.New("the program has started, try deleting ~/.ggtsync or killing the program and try again"))
		os.Exit(1)
	} else if err != config.ErrorConfigNotRunning {
		fmt.Fprintf(os.Stderr, configStartFailure, err)
		os.Exit(1)
	}

	config.Deamon()
	err = config.Load(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, configStartFailure, err)
		config.Close()
		os.Exit(1)
	}

	projects := config.GetProjects()
	for _, project := range projects {
		handler.Handle(&project)
	}

	handler.Wait()
}

const (
	configReloadSuccess = `ggtsync: config successfully load
`
	configReloadFailure = `ggtsync: %v
ggtsync: fail to reload config
`
)

func reload() {
	pid, err := config.GetPid()
	if err != nil {
		fmt.Fprintf(os.Stderr, configReloadFailure, err)
		os.Exit(1)
	}

	err = syscall.Kill(pid, syscall.SIGUSR1)
	if err != nil {
		fmt.Fprintf(os.Stderr, configReloadFailure, err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, configReloadSuccess)
	os.Exit(0)
}

const (
	configQuitSuccess = `ggtsync: successfully quit
`
	configQuitFailure = `ggtsync: %v
ggtsync: fail to quit, please use "kill" command to exit manually
`
)

func quit() {
	pid, err := config.GetPid()
	if err != nil {
		fmt.Fprintf(os.Stderr, configQuitFailure, err)
		os.Exit(1)
	}

	err = syscall.Kill(pid, syscall.SIGUSR2)
	if err != nil {
		fmt.Fprintf(os.Stderr, configQuitFailure, err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, configQuitSuccess)
	os.Exit(0)
}

// Signal -
// TODO:
func Signal(signal, filename string) {
	switch signal {
	case "start":
		start(filename)
	case "reload":
		reload()
	case "quit":
		quit()
	default:
		help()
	}
}
