package command

import (
	"fmt"
	"os"
)

func help() string {
	return `Usage: ggtsync [-hvt] [-s signal] [-c filename]

Options:
  -h          : this help
  -v          : show version and exit
  -t          : test configuration and exit
  -s signal   : send signal to a master process: start, quit, reload (default "start")
  -c filename : set configuration file (default "config.yaml")
`
}

// Help -
// TODO:
func Help() {
	fmt.Fprintf(os.Stdout, "%s%s", version(), help())
	os.Exit(0)
}
