package command

import (
	"fmt"
	"os"

	"gitlab.com/banggua/ggtsync/config"
)

const (
	testSuccess = `ggtsync: the configuration file %s syntax is ok
ggtsync: the configuration file %s test is successful
`
	testFailure = `ggtsync: %v
ggtsync: the configuration file %s test failed
`
)

func test(filename string) error {
	return config.Load(filename)
}

// Test -
// TODO:
func Test(filename string) {
	err := test(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, testFailure, err, filename)
		os.Exit(1)
	}
	fmt.Fprintf(os.Stdout, testSuccess, filename, filename)
	os.Exit(0)
}
